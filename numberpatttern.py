def pattern1(n):
    x = 0
    for i in range(0,n):
        x += 1
        for j in range(0, i+1):
            print(x, end=" ")
        print("\r")
pattern1(5)   

def pattern2(n):
    for i in range(0,n):
        for j in range(1, i+2):
            print(j, end=" ")
        print("\r")
pattern2(5)


def pattern(n):
    for i in range(0,n):
        for j in range(1, i+2):
            print(j, end=" ")
        print("\r")

    for i in range(n,0,-1):
        for j in range(i-1,0,-1):
            print(j, end=" ")
        print("\r")     
pattern(5)